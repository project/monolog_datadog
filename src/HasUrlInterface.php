<?php

declare(strict_types=1);

namespace Drupal\monolog_datadog;

/**
 * Defines an interface for an object with a URL.
 */
interface HasUrlInterface {

  /**
   * Get the URL.
   *
   * @return string
   *   The URL.
   */
  public function getUrl(): string;

}
