<?php

declare(strict_types=1);

namespace Drupal\monolog_datadog\Monolog\Handler;

use Drupal\Core\Config\ConfigFactory;
use Drupal\monolog_datadog\DatadogRegion;
use Drupal\monolog_datadog\HasUrlInterface;
use Drupal\monolog_datadog\Monolog\Processor\DatadogLevelProcessor;
use Monolog\Formatter\FormatterInterface;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Handler\MissingExtensionException;
use Monolog\Level;
use Monolog\LogRecord;

/**
 * Sends logs to Datadog Logs using Curl integrations.
 *
 * You'll need a Datadog account to use this handler.
 */
final class DatadogHandler extends AbstractProcessingHandler {

  /**
   * CURL error codes.
   *
   * @var array
   */
  private const RETRIABLE_ERROR_CODES = [
    CURLE_COULDNT_RESOLVE_HOST,
    CURLE_COULDNT_CONNECT,
    CURLE_HTTP_NOT_FOUND,
    CURLE_READ_ERROR,
    CURLE_OPERATION_TIMEOUTED,
    CURLE_HTTP_POST_ERROR,
    CURLE_SSL_CONNECT_ERROR,
  ];

  /**
   * Datadog Api Key access.
   *
   * @var string
   */
  private string $apiKey;

  /**
   * Datadog DD Tags.
   *
   * @var string
   */
  private string $ddtags;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\monolog_datadog\HasUrlInterface|string $region
   *   Datadog region.
   * @param \Monolog\Level|string $level
   *   The minimum logging level at which this handler will be triggered.
   * @param array $attributes
   *   Some options fore Datadog Logs.
   * @param bool $bubble
   *   Whether the messages that are handled can bubble up the stack or not.
   *
   * @throws \Monolog\Handler\MissingExtensionException
   */
  public function __construct(
    private readonly ConfigFactory $configFactory,
    private HasUrlInterface|string $region,
    Level|string $level = Level::Debug,
    private readonly array $attributes = [],
    bool $bubble = TRUE,
  ) {
    if (!extension_loaded('curl')) {
      throw new MissingExtensionException('The curl extension is needed to use the DatadogHandler');
    }
    // To ease instantiation if difficulty instantiating with enums.
    if (is_string($level)) {
      $level = Level::fromName($level);
    }
    if (is_string($region)) {
      $this->region = DatadogRegion::from($this->region);
    }
    parent::__construct($level, $bubble);
    $config = $this->configFactory->get('monolog_datadog.settings');
    $this->ddtags = $config->get('ddtags');
    $this->apiKey = $config->get('api_key');
    $this->pushProcessor(new DatadogLevelProcessor());
  }

  /**
   * Handles a log record.
   *
   * @param \Monolog\LogRecord $record
   *   The record.
   *
   * @throws \Exception
   */
  protected function write(LogRecord $record): void {
    $this->send($record->formatted);
  }

  /**
   * Send request to the regional URL.
   *
   * @param string $record
   *   The record.
   */
  protected function send(string $record): void {
    $headers = [
      'Content-Type:application/json',
      sprintf('DD-API-KEY:%s', $this->apiKey),
    ];
    $source = urlencode($this->getSource());
    $hostname = urlencode($this->getHostname());
    $service = urlencode($this->getService($record));
    $record = $this->setStatus($record);

    $url = sprintf(
      '%s?ddsource=%s&service=%s&hostname=%s&ddtags=%s',
      $this->region->getUrl(),
      $source,
      $service,
      $hostname,
      urlencode($this->ddtags)
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $record);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    $this->execute($ch);
  }

  /**
   * Get Datadog Api Key from $attributes params.
   *
   * @param string $apiKey
   *   The api key.
   */
  protected function getApiKey(string $apiKey): string {
    if ($apiKey) {
      return $apiKey;
    }
    else {
      throw new \Exception('The Datadog Api Key is required');
    }
  }

  /**
   * Get Datadog Source from $attributes params.
   *
   * @return mixed|string
   *   Mixed.
   */
  protected function getSource() {
    return !empty($this->attributes['source']) ? $this->attributes['source'] : 'php';
  }

  /**
   * Get service.
   *
   * @param string $record
   *   The record.
   *
   * @return mixed
   *   Mixed.
   */
  protected function getService(string $record) {
    $channel = json_decode($record, TRUE);

    return !empty($this->attributes['service']) ? $this->attributes['service'] : $channel['channel'];
  }

  protected function setStatus(string $record) {

    $data = json_decode($record, TRUE);

    // The attribute "status" is a reserved attribute in Datadog. We can forward the level_name to status.
    // See https://docs.datadoghq.com/logs/log_configuration/attributes_naming_convention/#reserved-attributes
    if (empty($data['status']) && !empty($data['level'])) {
      $data['status'] = $data['level_name'];
    }
    return json_encode($data);
  }

  /**
   * Get Datadog Hostname from $attributes params.
   */
  protected function getHostname() {
    return !empty($this->attributes['hostname']) ? $this->attributes['hostname'] : $_SERVER['SERVER_NAME'];
  }

  /**
   * Returns the default formatter to use with this handler.
   *
   * @return \Monolog\Formatter\JsonFormatter
   *   Returns formatter.
   */
  protected function getDefaultFormatter(): FormatterInterface {
    return new JsonFormatter();
  }

  /**
   * Executes a CURL request with optional retries and exception on failure.
   *
   * Copied from Util::execute() as this Util is internal.
   *
   * @param \CurlHandle $ch
   *   Curl handler.
   * @param int $retries
   *   The number of retries.
   * @param bool $closeAfterDone
   *   Close after done.
   */
  public function execute(\CurlHandle $ch, int $retries = 5, bool $closeAfterDone = TRUE): bool|string {
    while ($retries--) {
      $curlResponse = curl_exec($ch);
      if ($curlResponse === FALSE) {
        $curlErrno = curl_errno($ch);
        if (FALSE === in_array($curlErrno, self::RETRIABLE_ERROR_CODES, TRUE) || $retries === 0) {
          $curlError = curl_error($ch);
          if ($closeAfterDone) {
            curl_close($ch);
          }
          throw new \RuntimeException(sprintf('Curl error (code %d): %s', $curlErrno, $curlError));
        }
        continue;
      }
      if ($closeAfterDone) {
        curl_close($ch);
      }
      return $curlResponse;
    }

    return FALSE;
  }

}
