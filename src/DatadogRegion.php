<?php

declare(strict_types=1);

namespace Drupal\monolog_datadog;

/**
 * The Datadog Regions.
 */
enum DatadogRegion: string implements HasUrlInterface {
  case US = 'US';
  case US3 = 'US3';
  case US5 = 'US5';
  case EU = 'EU';
  case US1_FED = 'US1-FED';

  /**
   * Get the log host URL.
   *
   * @return string
   *   The host URL.
   */
  public function getUrl(): string {
    return match($this) {
      self::US => 'https://http-intake.logs.datadoghq.com/api/v2/logs',
      self::US3 => 'https://http-intake.logs.us3.datadoghq.com/api/v2/logs',
      self::US5 => 'https://http-intake.logs.us5.datadoghq.com/api/v2/logs',
      self::EU => 'https://http-intake.logs.datadoghq.eu/api/v2/logs',
      self::US1_FED => 'https://http-intake.logs.ddog-gov.com/api/v2/logs'
    };
  }

  /**
   * Get the region by value.
   *
   * @param string $value
   *   Region value.
   *
   * @return self
   *   The region.
   */
  public static function fromValue(string $value): self {
    foreach (self::cases() as $case) {
      if ($case->value === $value) {
        return $case;
      }
    }
    throw new \InvalidArgumentException(
      sprintf(
        'Not a valid Datadog region. Must be one of %s.',
        implode(
          ', ',
          array_map(static function (self $case) {
            return $case->value;
          }, self::cases())))
    );
  }

}
