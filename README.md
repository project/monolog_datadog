# Monolog Datadog

Uses a Monolog handler to send Logs to Datadog without a Datadog agent.

## Usage

`docroot/sites/default/logging.services.yml`

```yaml
parameters:
  datadog.region: 'EU'
  datadog.level: 'DEBUG'
  monolog.channel_handlers:
    default: ['datadog', 'drupal.dblog']
    php: ['datadog', 'drupal.dblog']

services:
  monolog.handler.datadog:
    class: Drupal\monolog_datadog\Monolog\Handler\DatadogHandler
    arguments:
      - '@config.factory'
      - '%datadog.region%'
      - '%datadog.level%'
```

### API key

Set the API key in your `settings.php` like

```php
$config['monolog_datadog.settings']['api_key'] = getenv('DD_API_KEY') ?: 'Invalid';
```

### Datadog Tags

Set tags like `env` or custom tags like `project` also in your `settings.php`

```php
$config['monolog_datadog.settings']['ddtags'] = 'env:production,project:whatever';
```
